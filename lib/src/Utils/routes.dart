import 'package:flutter/widgets.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_anonimos.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_cnombre.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_corrupcion.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_institucion.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_total.dart';
import 'package:movil_corruptometro/src/views/view_denunciar/denuncia_cnombre.dart';
import 'package:movil_corruptometro/src/views/view_denunciar/denuncia_form.dart';
import 'package:movil_corruptometro/src/views/view_principal.dart';
// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  ViewPrincipal.routeName : (context) => ViewPrincipal(),
  DenunciarForm.routeName : (context) => DenunciarForm(),
  CasosCorrupcion.routeName : (context) => CasosCorrupcion(),
  DenunciaCNombre.routeName : (context) => DenunciaCNombre(),
  TotalCasos.routeName : (context) => TotalCasos(),
  CasosInstitcuon.routeName : (context) => CasosInstitcuon(),
  CasosAnonimos.routeName : (context) => CasosAnonimos(),
  CasosCNombre.routeName : (context) => CasosCNombre()
  
};
