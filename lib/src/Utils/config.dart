
const String api = 'http://fintech.kradac.com:3005/api-corruptometro/';
const String ocurrioUnError = 'Ocurrio un error, intenta más tarde.';
const int codRespCorrecta = 1;
const int codRespError = 0;
const String personaLibro = 'assets/images/persona-libro.png';
const String corruptometro = 'assets/images/corruptometro.png';
const String fcaje = 'assets/images/fcaje.png';
const String funda = 'assets/images/FUNDA.png';
const String mapa = 'assets/images/mapa.png';
const String denunciar = 'assets/images/denunciar.png';