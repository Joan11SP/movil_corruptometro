import 'package:flutter/material.dart';
ThemeData theme() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    fontFamily: "Times New Roman",
    appBarTheme: appBarTheme(),
    //textTheme: textTheme(),
    inputDecorationTheme: inputDecorationTheme(),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
  );
}

InputDecorationTheme inputDecorationTheme() {
  OutlineInputBorder outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(18),  
    borderSide: BorderSide(color: Colors.grey),
    gapPadding: 5,
  );
  return InputDecorationTheme(
    floatingLabelBehavior: FloatingLabelBehavior.always,
    contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 16),
    enabledBorder: outlineInputBorder,
    border: outlineInputBorder,
  );
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    elevation: 1,
    brightness: Brightness.dark,
    //iconTheme: IconThemeData(color: colorSecundario),
    textTheme: TextTheme(
      headline6: TextStyle(color: Color(0XFF8B8B8B), fontSize: 18),
    ),
  );
}
