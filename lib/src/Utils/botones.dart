import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

void mostrarLoading(BuildContext context, [String text]) {
  text = text == null ? 'Cargando...' : text;
  showDialog(
    //barrierColor: Colors.black.withOpacity(0.7),
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        content: Container(
          color: Colors.transparent,
          padding: EdgeInsets.all(10.0),
          child: new Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(
                backgroundColor: Colors.white,
                strokeWidth: 1,
              ),
              SizedBox(
                height: 10.0,
              ),
              new Text(
                text,
                style: TextStyle(color: Colors.white, fontFamily: 'semibold'),
              ),
            ],
          ),
        ),
      );
    },
  );
}

mostrarMensaje(String msg, context, [int duracion]) {
  duracion = duracion == null ? 3 : duracion;
  return Toast.show(msg, context, duration: duracion);
}

esconderLoading(context) {
  Navigator.of(context).pop();
}


TextStyle estiloText([double textSize]){
  return TextStyle(
    color: Colors.white,
    fontSize: textSize == null ? 17.0 : textSize,
    fontWeight: FontWeight.w300,
  ); 
}
TextStyle estiloText2([double textSize]){
  return TextStyle(
    color: Colors.black,
    fontSize: textSize == null ? 17.0 : textSize,
    fontWeight: FontWeight.w300,
  ); 
}
hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }