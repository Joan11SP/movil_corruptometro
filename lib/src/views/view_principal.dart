import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/config.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_corrupcion.dart';
import 'package:movil_corruptometro/src/views/view_denunciar/denuncia_form.dart';

class ViewPrincipal extends StatelessWidget {
  static String routeName = '/principal';
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              mostrarImagen(fcaje, 250, 200),
              mostrarImagen(corruptometro, 200, 100),
              SizedBox(
                height: getProportionateScreenHeight(25),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  mostrarImagen(personaLibro, 200, 150),
                  SizedBox(
                    width: getProportionateScreenWidth(5),
                  ),
                  mostrarImagen(denunciar, 150, 150),
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              Padding(
                padding: EdgeInsets.only(left: 25, right: 25),
                child: Container(
                  height: getProportionateScreenHeight(75),
                  child: Card(                  
                    color: Colors.white,
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          child: Container(
                              child: Text(
                            'Casos corrupción',
                            style: TextStyle(fontSize: 15),
                          )),
                          onTap: () => Navigator.pushNamed(
                              context, CasosCorrupcion.routeName),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(5),
                        ),
                        Container(
                          color: Colors.black,
                          height: getProportionateScreenHeight(50),
                          width: getProportionateScreenWidth(1),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(5),
                        ),
                        GestureDetector(
                          child: Container(
                            child: Text('Denuncia corrupción',
                                style: TextStyle(fontSize: 15)),
                          ),
                          onTap: () => Navigator.pushNamed(
                              context, DenunciarForm.routeName),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(25),
              ),
              mostrarImagen(funda, 200, 150),
            ],
          ),
        ),
      ),
    );
  }

  mostrarImagen(String img, double w, double h) {
    return Container(
      child: Image.asset(
        img,
        width: getProportionateScreenWidth(w),
        height: getProportionateScreenHeight(h),
      ),
    );
  }
}
