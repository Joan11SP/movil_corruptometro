import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/api/api-corruptometro.dart';

class CasosCNombre extends StatefulWidget {
  static String routeName = '/casos-cnombre';
  @override
  _CasosCNombreState createState() => _CasosCNombreState();
}

class _CasosCNombreState extends State<CasosCNombre> {

  var cnombre = [];

  getAnonimas() async {
    await Future.delayed(Duration(microseconds:2));
    var respuesta = await obtenerRecurso('buscar-no_anonima', {}, context);
    if (respuesta !=-1 ){
      cnombre = respuesta['resultados'];
      setState(() {});
    }
  }

  @override
  void initState() {
    getAnonimas();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: getProportionateScreenHeight(25),),
              Text('Casos con denunciante',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              SizedBox(height: getProportionateScreenHeight(25),),
              cnombre.length<=0
              ? Text('')              
              :Text('Total de Casos: '+ cnombre.length.toString(),style: TextStyle(fontSize: 18,fontWeight: FontWeight.w300)),
              cnombre.length<=0
              ? Padding(
                padding: EdgeInsets.only(top:8.0),
                child: Text('No existen casos denunciados'),
              )
              : 
              SizedBox(height: getProportionateScreenHeight(25),),
              Center(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: cnombre.length,
                  itemBuilder: (context,i){
                    return Padding(
                      padding: EdgeInsets.only(bottom: 15,left: 5,right: 5),
                      child: Card(                  
                        color: Colors.white,
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                mostrarTexto(Icons.person, 'Denunciante'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['nombre_denunciante'].toString()),
                                ),
                                mostrarTexto(Icons.location_city, 'Ciudad'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['ciudad'].toString()),
                                ),
                                SizedBox(height: getProportionateScreenHeight(15),),
                                mostrarTexto(Icons.location_city_outlined, 'Provincia'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['provincia'].toString()),
                                ),
                                SizedBox(height: getProportionateScreenHeight(15),),
                                mostrarTexto(Icons.location_city_outlined, 'Institución'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['institucion'].toString()),
                                ),
                                SizedBox(height: getProportionateScreenHeight(15),),
                                mostrarTexto(Icons.gavel_rounded, 'Tipo de corrupción'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['tipoCorrupcion'].toString()),
                                ),
                                SizedBox(height: getProportionateScreenHeight(15),),
                                mostrarTexto(Icons.date_range_sharp, 'Fecha'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['fecha'].toString()),
                                ),
                                SizedBox(height: getProportionateScreenHeight(15),),
                                mostrarTexto(Icons.details, 'Detalles'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['detalles_denuncia'].toString()),
                                ),
                                SizedBox(height: getProportionateScreenHeight(15),),
                                mostrarTexto(Icons.description, 'Descripción'),
                                Padding(
                                  padding: EdgeInsets.only(left: getProportionateScreenWidth(35)),
                                  child: Text(cnombre[i]['descripcion'].toString()),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  } 
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  mostrarTexto(IconData icon, String nombre){
    return Row(
      children: [
        Icon(icon,color: Colors.lightBlue[400],),
        SizedBox(width:getProportionateScreenWidth(10),),
        Text(nombre,style: TextStyle(fontWeight: FontWeight.bold),)
      ],
    );
  }
}