import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/botones.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_anonimos.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_cnombre.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_total.dart';

class CasosCorrupcion extends StatefulWidget {
  static String routeName = '/casos-corrupcion';
  @override
  _CasosCorrupcionState createState() => _CasosCorrupcionState();
}

class _CasosCorrupcionState extends State<CasosCorrupcion> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: getProportionateScreenHeight(50)),
              Text('Casos corrupción',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){Navigator.pushNamed(context, TotalCasos.routeName);}, 'Total de Casos'),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){Navigator.pushNamed(context, CasosAnonimos.routeName);}, 'Casos anónimos'),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){Navigator.pushNamed(context, CasosCNombre.routeName);}, 'Denuncias con nombre'),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){mostrarMensaje('En Desarrollo', context);}, 'Provincias'),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){mostrarMensaje('En Desarrollo', context);}, 'Institución'),
            ],
          ),
        ),
      ),
    );
  }

  opcionBoton(Function press, String text) {
    return Center(
      child: MaterialButton(
        height: getProportionateScreenHeight(100),
        minWidth: getProportionateScreenWidth(250),
        onPressed: press,
        child: Text(text,style: TextStyle(fontSize: 18),),
        color: Colors.white,
        elevation: 7,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
    );
  }
}
