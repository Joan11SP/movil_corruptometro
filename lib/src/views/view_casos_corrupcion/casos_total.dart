import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/botones.dart';
import 'package:movil_corruptometro/src/Utils/config.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/views/view_casos_corrupcion/casos_institucion.dart';

class TotalCasos extends StatefulWidget {
  static String routeName = '/total-casos';
  @override
  _TotalCasosState createState() => _TotalCasosState();
}

class _TotalCasosState extends State<TotalCasos> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: getProportionateScreenHeight(30)),
              Text('Total de Casos',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              SizedBox(height: getProportionateScreenHeight(20)),
              mostrarImagen(mapa, 600, 300),
              SizedBox(height: getProportionateScreenHeight(20)),
              opcionBoton((){Navigator.pushNamed(context, CasosInstitcuon.routeName);}, 'Top 10 instituciones'),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){mostrarMensaje('En Desarrollo', context);}, 'Galeria de fotos'),
              SizedBox(height: getProportionateScreenHeight(30)),
              opcionBoton((){mostrarMensaje('En Desarrollo', context);}, 'Videos Cargados'),
            ],
          ),
        ),
      ),
    );
  }

  opcionBoton(Function press, String text) {
    return Center(
      child: MaterialButton(
        height: getProportionateScreenHeight(100),
        minWidth: getProportionateScreenWidth(250),
        onPressed: press,
        child: Text(text,style: TextStyle(fontSize: 18),),
        color: Colors.white,
        elevation: 7,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
    );
  }
  mostrarImagen(String img, double w, double h) {
    return Container(
      child: Image.asset(
        img,
        width: getProportionateScreenWidth(w),
        height: getProportionateScreenHeight(h),
      ),
    );
  }
}
