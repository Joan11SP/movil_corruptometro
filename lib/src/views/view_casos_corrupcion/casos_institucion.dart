import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/api/api-corruptometro.dart';

class CasosInstitcuon extends StatefulWidget {
  static String routeName = '/casos-institucion';
  @override
  _CasosInstitcuonState createState() => _CasosInstitcuonState();
}

class _CasosInstitcuonState extends State<CasosInstitcuon> {

  var instituciones = [];

  getInstituciones() async {
    await Future.delayed(Duration(microseconds:2));
    var respuesta = await obtenerRecurso('filtro-institucion', {}, context);
    if (respuesta !=-1 ){
      instituciones = respuesta['resultados'];
      setState(() {});
    }
  }

  @override
  void initState() {
    getInstituciones();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: getProportionateScreenHeight(25),),
              Text('Top 10 Instituciones',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              instituciones.length<=0
              ? Padding(
                padding: EdgeInsets.only(top:8.0),
                child: Text('No existen instituciones denunciadas'),
              )
              : 
              SizedBox(height: getProportionateScreenHeight(25),),
              Center(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: instituciones.length,
                  itemBuilder: (context,i){
                    return Padding(
                      padding: EdgeInsets.only(bottom: 15,left: 5,right: 5),
                      child: Container(
                        height: getProportionateScreenHeight(100),
                        width: getProportionateScreenWidth(150),
                        child: Card(                  
                          color: Colors.white,
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Container(
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text(
                                  instituciones[i]['nombre'].toString() + ' - ' + instituciones[i]['total'].toString()+ ' Casos',
                                  style: TextStyle(fontSize: 18),),
                              )
                              ),
                          ),
                      ),
                  ),
                    );
                  } 
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}