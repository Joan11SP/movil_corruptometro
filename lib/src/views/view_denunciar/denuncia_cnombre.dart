import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/botones.dart';
import 'package:movil_corruptometro/src/Utils/config.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/views/view_denunciar/denuncia_controller.dart';

class DenunciaCNombre extends StatefulWidget {
  static String routeName = '/denuncia_cnombre';
  @override
  _DenunciaCNombreState createState() => _DenunciaCNombreState();
}

class _DenunciaCNombreState extends State<DenunciaCNombre> {
  final TextEditingController textNombre = TextEditingController();
  final TextEditingController textEdad = TextEditingController();
  final TextEditingController textGenero = TextEditingController();
  final TextEditingController textProfesion = TextEditingController();
  final TextEditingController textNombred = TextEditingController();
  final TextEditingController textCargod = TextEditingController();
  var datosEnviar;
  var genero = [{'id':1,'valor':'Hombre'}, {'id':2,'valor':'Mujer'}, {'id':3,'valor':'Otro'}];
  var id;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    datosEnviar = ModalRoute.of(context).settings.arguments;

    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
          child: Container(
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: getProportionateScreenHeight(35),
                  ),
                  Text('¿Denunciante?',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  mostrarText(textNombre, 'Nombre'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  campoNumerico(textEdad, 'Edad'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposLista(textGenero, () => mostrarTipoGenero(), 'Genero'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  mostrarText(textProfesion, 'Profesión'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  Text('¿A quién denuncia?',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  mostrarText(textNombred, 'Nombre'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  mostrarText(textCargod, 'Cargo'),
                  SizedBox(
                    height: getProportionateScreenHeight(15),
                  ),
                  RaisedButton(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 5,
                    onPressed: () { dialogDenuncia();},
                    child: Text(
                      'Enviar',
                      style: TextStyle(
                          fontSize: 23, fontWeight: FontWeight.normal),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      )),
    );
  }

  mostrarText(TextEditingController text, String nameForm, [int tipo]) {
    return TextFormField(
      controller: text,
      decoration: InputDecoration(
        labelText: nameForm,
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }
  campoNumerico(TextEditingController text, String nameForm){
    return TextFormField(
      controller: text,
      keyboardType: TextInputType.number,
      validator: (value){
        int val = int.parse(value);
        if(val > 75 || val < 18){
          return 'Valor no permitido';
        }
        return '';
      },
      decoration: InputDecoration(
        labelText: nameForm,
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );


  }

  camposLista(TextEditingController controller, Function fun, String text) {
    return GestureDetector(
      onTap: fun,
      child: TextFormField(
        controller: controller,
        enabled: false,
        onChanged: (String value) {
          setState(() {
            textGenero.text = value;
          });
        },
        decoration: InputDecoration(
          labelText: text,
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
      ),
    );
  }

  void mostrarTipoGenero() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          content: Container(
            color: Colors.transparent,
            padding: EdgeInsets.all(10.0),
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: genero.length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        textGenero.text = genero[i]['valor'].toString();
                        id = genero[i]['id'];
                      });
                      Navigator.of(context).pop();
                    },
                    child: Container(
                        padding: EdgeInsets.only(bottom: 12, top: 5),
                        child: Column(
                          children: [
                            Text(genero[i]['valor'].toString()),
                            Text(
                              '--------------------',
                              style: TextStyle(
                                fontWeight: FontWeight.w200,
                              ),
                            ),
                          ],
                        )),
                  );
                }),
          ),
        );
      },
    );
  }

  bool validarDatos() {
    bool val = false;
    if (textGenero.text.length>0 && textEdad.text.length>0 && textNombre.text.length>0 
          && textNombred.text.length>0 && textProfesion.text.length>0 && textCargod.text.length>0) 
    {
      val = true;
    }
    else
      mostrarMensaje('Completa la información', context);
    return val;
  }

  obtenerDatosFormulario() async {
    
    datosEnviar['edad'] = textEdad.text.toString();
    datosEnviar['genero'] = id.toString();
    datosEnviar['nombre'] = textNombre.text;
    datosEnviar['profesion'] = textProfesion.text;
    datosEnviar['denunciado'] = textNombred.text;
    datosEnviar['cargo'] = textCargod.text;
    print(datosEnviar);
    try {
      var img = {'imagen': datosEnviar['img'],'extension': datosEnviar['ext'] };
      bool data = validarDatos(); 
      if (data){
        mostrarLoading(context);
        await controllerGuardarDenuncia(datosEnviar, img, context);

      }
    } catch (e) {
      esconderLoading(context);
      mostrarMensaje(ocurrioUnError, context);
    }
  }
  dialogDenuncia() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Denunciar",
            ),
            content: Text(
              '¿Esta seguro/a de realizar la denuncia con sus datos?',
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop(context);
                },
              ),
              FlatButton(
                child: Text('Aceptar'),
                onPressed: () {
                   obtenerDatosFormulario();
                },
              )
            ],
          );
        });
  }

}
