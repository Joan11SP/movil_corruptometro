import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movil_corruptometro/src/Utils/botones.dart';
import 'package:movil_corruptometro/src/Utils/config.dart';
import 'package:movil_corruptometro/src/Utils/size_config.dart';
import 'package:movil_corruptometro/src/views/view_denunciar/denuncia_cnombre.dart';
import 'package:movil_corruptometro/src/views/view_denunciar/denuncia_controller.dart';
import 'package:path/path.dart' as path;
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class DenunciarForm extends StatefulWidget {
  static String routeName = '/denunciar-form';
  @override
  _DenunciarFormState createState() => _DenunciarFormState();
}

class _DenunciarFormState extends State<DenunciarForm> {
  final TextEditingController textCiudad = TextEditingController();
  final TextEditingController textProvincia = TextEditingController();
  final TextEditingController textInstitucion = TextEditingController();
  final TextEditingController textTipoCorrupcion = TextEditingController();
  final TextEditingController textCorreo = TextEditingController();
  final TextEditingController textDescripcion = TextEditingController();
  final TextEditingController textDetalle = TextEditingController();
  final TextEditingController textFecha = TextEditingController();

  Map imagenes = {'imagen': null};
  var city;
  File imagen;
  var obtenerId = {};
  final picker = ImagePicker();
  bool addArchivos = true;
  double _lowerValue = 1, _upperValue = 1;
  getDatos() async {
    await Future.delayed(Duration(milliseconds: 2));
    city = await controllerGetCiudades(context);
    if (city.length > 0) {
      textCiudad.text = city[0][0]['nombre'].toString();
      textProvincia.text = city[1][0]['nombre'].toString();
      obtenerId['id_provincia'] = city[1][0]['id_provincia'].toString();
      obtenerId['id_ciudad'] = city[0][0]['id_ciudad'].toString();
    }
  }

  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    getDatos();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposLista(textProvincia, () {
                    if (city[1].length > 1) {
                      mostrarProvincias();
                    }
                  }, 'Provincia', false),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposLista(textCiudad, () {}, 'Ciudad', false),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposLista(textInstitucion, () {
                    if (city[2].length > 0) {
                      mostrarInstitucion();
                    }
                  }, 'Institución', false),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposFormulario(textDetalle, '',
                      'Oficina, departamento, etc'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposLista(textFecha, () {
                    _selectDate(textFecha);
                  }, 'Fecha', false),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposLista(textTipoCorrupcion, () {
                    if (city[3].length > 0) {
                      mostrarTipoCorrupcion();
                    }
                  }, 'Tipo de Corrupción', false),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposFormulario(textDescripcion, 'Descripción',
                      'Breve descripción', 3),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  camposFormulario(textCorreo, 'Correo'),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  mostrarImagenes(),
                  SizedBox(height: getProportionateScreenHeight(25)),
                  Slider(
                    value: _upperValue,
                    divisions: 10,
                    min: 0,
                    max: 10,
                    label: 'Nivel de Corrupción $_upperValue',
                    onChanged: (value) {
                      setState(() {
                        _upperValue = value;
                      });
                    },
                    activeColor: _upperValue <= 4
                        ? Colors.green
                        : _upperValue > 4 && _upperValue < 7
                            ? Colors.yellow
                            : Colors.red,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RaisedButton(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        elevation: 5,
                        onPressed: () => dialogDenuncia(),
                        child: Text('Anónimo'),
                      ),
                      SizedBox(width: getProportionateScreenWidth(25)),
                      RaisedButton(
                        elevation: 5,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        onPressed: () => enviarDatos(),
                        child: Text('Mis datos \nNo tengo miedo'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => validar(),
        child: Icon(Icons.camera),
      ),
    );
  }

  camposFormulario(controller, String text,[String lbl, int lineas]) {
    return TextFormField(
      controller: controller,
      maxLines: lineas == null ? 1 : lineas,
      decoration: InputDecoration(
        labelText: text,
        hintText: lbl == null ? '' : lbl,
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  camposLista(
      TextEditingController controller, Function fun, String text, bool bl) {
    return GestureDetector(
      onTap: fun,
      child: TextFormField(
        controller: controller,
        enabled: bl,
        decoration: InputDecoration(
          labelText: text,
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
      ),
    );
  }

  mostrarImagenes() {
    return imagenes['imagen'] != null
        ? Container(
            child: Image.file(imagen),
            height: getProportionateScreenHeight(200),
            width: getProportionateScreenWidth(200),
          )
        : Container();
  }

  bool validarDatos() {
    bool val;
    if (textTipoCorrupcion.text.length > 0 &&
        textFecha.text.length > 0 &&
        textInstitucion.text.length > 0 && textDescripcion.text.length > 0 &&
        textDetalle.text.length > 0) {
      val = true;
    } else {
      mostrarMensaje('Completa la información', context);
      val = false;
    }
    print(textFecha.text.length);
    return val;
  }

  selectImageGallery() async {
    hideKeyboard(context);
    try {
      final img = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        imagen = File(img.path);
        var ext = path.extension(imagen.path);
        addArchivos = false;
        imagenes = {'imagen': imagen.path, 'extension': ext};
      });
      Navigator.pop(context);
    } on NoSuchMethodError catch (e) {
      Navigator.pop(context);
    } on PlatformException catch (e) {
      Navigator.pop(context);
    }
  }

  _selectDate(TextEditingController dia) async {
    hideKeyboard(context);
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime.now(),
        lastDate: DateTime(2050));
    if (picked != null)
      setState(() {
        dia.text = picked.year.toString() +
            "-" +
            picked.month.toString() +
            "-" +
            picked.day.toString();
      });
  }

  validar() async {
    mostrarLoading(context, 'Cargando...');
    var status = await Permission.storage.status;
    if (status.isGranted) {
      selectImageGallery();
    } else {
      mostrarMensaje('Habilita los permisos para continuar', context, 3);
      pedirPersmisos();
      Navigator.pop(context);
    }
  }

  obtenerDatosFormulario() async {
    obtenerId['detallesDenuncia'] = textDetalle.text;
    obtenerId['correo'] = textCorreo.text;
    obtenerId['nivelCorrupcion'] = _upperValue;
    obtenerId['descripcion'] = textDescripcion.text;
    obtenerId['fecha'] = textFecha.text;
    obtenerId['tipo'] = "1";
    try {
      bool data = validarDatos();
      if (data) {
        mostrarLoading(context);
        await controllerGuardarDenuncia(obtenerId, imagenes, context);
      }
      else
        esconderLoading(context);
    } on Exception catch (e) {
      esconderLoading(context);
      mostrarMensaje(ocurrioUnError, context);
    }
  }

  enviarDatos() {
    if (validarDatos()) {
      setState(() {
        obtenerId['tipo'] = "2";
        obtenerId['detallesDenuncia'] = textDetalle.text;
        obtenerId['correo'] = textCorreo.text;
        obtenerId['nivelCorrupcion'] = _upperValue;
        obtenerId['fecha'] = textFecha.text;
        obtenerId['descripcion'] = textDescripcion.text;
        obtenerId['img'] = imagenes['imagen'];
        obtenerId['ext'] = imagenes['extension'];
      });
      Navigator.of(context).pushNamed(DenunciaCNombre.routeName,arguments: obtenerId);
    }
  }

  void mostrarTipoCorrupcion() {
    hideKeyboard(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 0.0,
          content: Container(
            color: Colors.transparent,
            padding: EdgeInsets.all(10.0),
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: city[3].length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                      textTipoCorrupcion.text = city[3][i]['nombre'].toString();
                      Navigator.pop(context);
                      setState(() {
                        obtenerId['tipoCorrupcion'] =
                            city[3][i]['id_tipo'].toString();
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(bottom: 12, top: 5),
                      child: Text(city[3][i]['nombre'].toString()),
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  void mostrarProvincias() {
    hideKeyboard(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 0.0,
          content: Container(
            color: Colors.transparent,
            padding: EdgeInsets.all(10.0),
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: city[1].length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                      textProvincia.text = city[1][i]['nombre'].toString();
                      Navigator.pop(context);
                      hideKeyboard(context);
                    },
                    child: Container(
                      padding: EdgeInsets.only(bottom: 12, top: 5),
                      child: Text(city[1][i]['nombre'].toString()),
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  void mostrarInstitucion() {
    hideKeyboard(context);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 0.0,
          content: Container(
            color: Colors.transparent,
            padding: EdgeInsets.all(10.0),
            child: ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: city[2].length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                      textInstitucion.text = city[2][i]['nombre'].toString();
                      Navigator.pop(context);
                      setState(() {
                        obtenerId['idInstitucion'] =
                            city[2][i]['id_institucion'].toString();
                      });
                    },
                    child: Container(
                      padding: EdgeInsets.only(bottom: 12, top: 5),
                      child: Text(city[2][i]['nombre'].toString()),
                    ),
                  );
                }),
          ),
        );
      },
    );
  }

  dialogDenuncia() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Denunciar",
            ),
            content: Text(
              '¿Esta seguro/a de realizar la denuncia?',
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {
                  Navigator.of(context).pop(context);
                },
              ),
              FlatButton(
                child: Text('Aceptar'),
                onPressed: () {
                   obtenerDatosFormulario();
                },
              )
            ],
          );
        });
  }
}
