import 'dart:convert';

import 'package:movil_corruptometro/src/Utils/botones.dart';
import 'package:movil_corruptometro/src/Utils/config.dart';
import 'package:movil_corruptometro/src/api/api-corruptometro.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/views/view_principal.dart';

pedirPersmisos() async {
  Map<Permission, PermissionStatus> statuses = await [
    Permission.storage,
  ].request();
  return statuses;
}


controllerGuardarDenuncia(Map denuncia,imagenes,context) async {
  try {
    var url = api + 'nueva-denuncia';
    var reqs = http.MultipartRequest('POST', Uri.parse('$url'));
    print(denuncia);
    reqs.fields.addAll(
      {
        'detallesDenuncia' : denuncia['detallesDenuncia'],
        'correo' : denuncia['correo'].toString(),
        'nivelCorrupcion' : denuncia['nivelCorrupcion'].toString(),
        'descripcion' : denuncia['descripcion'],
        'fecha' : denuncia['fecha'],
        'id_provincia' : denuncia['id_provincia'].toString(),
        'id_ciudad' : denuncia['id_ciudad'].toString(),
        'tipoCorrupcion' : denuncia['tipoCorrupcion'],
        'idInstitucion' : denuncia['idInstitucion'],
        'tipo': denuncia['tipo'],
        'edad': denuncia['edad'].toString(),
        'genero':denuncia['genero'].toString(),
        'profesion':denuncia['profesion'].toString(),
        'nombre':denuncia['nombre'].toString(),
        'denunciado':denuncia['denunciado'].toString(),
        'idDenunciante' : '0',
        'esReal' : '1'
      }
    );
    if(imagenes["imagen"] != null){
       reqs.files.add(await http.MultipartFile.fromPath(
        'image',
        imagenes['imagen'],
        contentType: MediaType('image', imagenes['extension']),
      ));
    }
    //http.Response response = await crearCuestionarioApi(test);

    http.StreamedResponse res = await reqs.send();
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    var respuesta = json.decode(responseString);
    print(respuesta);
    esconderLoading(context);
    if (respuesta['ok'] == codRespCorrecta) {
      Navigator.popAndPushNamed(context,ViewPrincipal.routeName);
    }
    mostrarMensaje(respuesta['mensaje'], context, 3);
    
  } on Exception catch (e) {
    print(e);
    throw(e);
  }
}
controllerGetCiudades(context) async {
  var respuesta = await obtenerRecurso('datos-lista', {}, context);
  if(respuesta != -1){
    return respuesta['resultados'];
  }
  return [];
}