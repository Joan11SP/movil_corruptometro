import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:movil_corruptometro/src/Utils/botones.dart';
import 'package:movil_corruptometro/src/Utils/config.dart';

var header = {
  "content-type": "application/json",
  'app':'app_niniera'
};
var headerNoJosn= {
  'app':'app_corruptometro'
};

dynamic obtenerRecurso(String ruta,body, context) async {
  dynamic datosRecurso;
  mostrarLoading(context);
  try {
    http.Response response = await http.post(api+ruta, body: body,headers: headerNoJosn);
    
    datosRecurso = json.decode(response.body);
    
    esconderLoading(context);

    if(datosRecurso['ok'] == codRespError){
      mostrarMensaje(datosRecurso['mensaje'], context);
      return datosRecurso = -1;
    }
    else
      return datosRecurso;

  } on Exception catch (e) {
    print(e);
    esconderLoading(context);
    mostrarMensaje(ocurrioUnError, context);
    return datosRecurso = -1;
  }
}