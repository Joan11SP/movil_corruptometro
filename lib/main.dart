import 'package:flutter/material.dart';
import 'package:movil_corruptometro/src/Utils/routes.dart';
import 'package:movil_corruptometro/src/Utils/theme.dart';
import 'package:movil_corruptometro/src/views/view_principal.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Corruptometro',
      home: ViewPrincipal(),
      routes: routes,
      theme: theme(),
    );
  }
}

